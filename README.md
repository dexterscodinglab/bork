# bork
Bork is a simple wrapper for Borg. It simplifies the use of borg to allow you to run bork once to configure it with your Borg repository, and after that it can be used with it's corresponding flags for less typing.

## Requirements
To use Bork, you need to have the following installed:
- BorgBackup
- SSH

## Usage
To initialize Bork, simply run `sudo ./bork` without any flags and follow the Setup Wizard. Bork will automatically setup a new repository for use on the target backup server.

Sudo privileges are optional when initializing and when creating a backup, but if you want to backup system files such as /etc/ then it will be required to run bork with sudo. By default, the backup config gets written to /$HOME/.backup_config, but this can be changed by editing the bork executable.

After Bork has been initialized, you can use the following flags.

## Creating a Backup
Creating backups is simple. Run `bork --create`.

## Listing Backups
To see available backups for restoration, run `bork --list` or `bork --list <backup name> to list a specific backup and it's content.

## Deleting Backups
To delete a backup, run `bork --delete` or `bork --delete <backup name>` to delete a specific backup.

## Configuration
After the initialization, it's possible to change the configured repository or directories to archive by editing the configuration file (defaults to /$HOME/.backup_config).
